//
//  GeometryPieceDescriptor.swift
//  Porcelain
//
//  Created by Mateusz Stompór on 18/11/2020.
//

import Foundation

public struct GeometryPieceDescriptor {
    // MARK: - Properties
    public let geometry: Int
    public let drawDescriptor: Int
}
