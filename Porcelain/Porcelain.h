//
//  Porcelain.h
//  Porcelain
//
//  Created by Mateusz Stompór on 05/11/2020.
//

#import <Foundation/Foundation.h>

//! Project version number for Porcelain.
FOUNDATION_EXPORT double PorcelainVersionNumber;

//! Project version string for Porcelain.
FOUNDATION_EXPORT const unsigned char PorcelainVersionString[];
